const navAnimation = () => {
    const bar = document.querySelector('.bar');
    const nav = document.querySelector('.nav-links');
    const navlinks = document.querySelectorAll('.nav-links li');
    bar.addEventListener('click',() => {
        nav.classList.toggle('nav-active');
        navlinks.forEach((link, index) => {
            if(link.style.animation) {
                link.style.animation="";
            } else {
                link.style.animation=`navSlideIn 0.5s ease forwards ${index/5 + 0.2}s`;
            }
        });
        bar.classList.toggle('toggle');
    });
    const navbar = document.querySelector('nav');
    navbar.style.position = "fixed";
    var navele1 = document.querySelector('.home-nav');
    navele1.addEventListener("mouseover", ()=> {
        navele1.style.color = 'black';
    });
    navele1.addEventListener("mouseout", ()=> {
        navele1.style.color = 'white';
    });
    var navele2 = document.querySelector('.about-nav');
    navele2.addEventListener("mouseover", ()=> {
        navele2.style.color = 'black';
    });
    navele2.addEventListener("mouseout", ()=> {
        navele2.style.color = 'white';
    });
    var navele3 = document.querySelector('.contact-nav');
    navele3.addEventListener("mouseover", ()=> {
        navele3.style.color = 'black';
    });
    navele3.addEventListener("mouseout", ()=> {
        navele3.style.color = 'white';
    });
    var navele4 = document.querySelector('.feedback-nav');
    navele4.addEventListener("mouseover", ()=> {
        navele4.style.color = 'black';
    });
    navele4.addEventListener("mouseout", ()=> {
        navele4.style.color = 'white';
    });
    document.querySelector('#active').addEventListener("mouseout", () => {
        document.querySelector('#active').style.color = 'yellow';
    });
}
navAnimation();

var imgarr = new Array();
imgarr[0] = "../IMG/karnataka.png";
imgarr[1] = "../IMG/bangalore.jpg";
imgarr[2] = "../IMG/udupi.jpg";
imgarr[3] = "../IMG/ChitradurgaFort1.jpg";
imgarr[4] = "../IMG/davangere.jpg";
imgarr[5] = "../IMG/kathakali.jpg";
imgarr[6] = "../IMG/temple.jpg";
imgarr[7] = "../IMG/mysorefort.jpg";
var cur = 0;
var last = imgarr.length-1;
var delaytime = 500;
function slideanimation() {
    document.getElementById('imgarea').classList.remove('slidein');
    if(cur === last) {
        cur = 0;
    } else {
        cur++;
    }
    document.getElementById('imgarea').src = imgarr[cur];
    document.getElementById('imgarea').classList.add('slidein');
    setTimeout(function() {
        document.getElementById('imgarea').classList.remove('slidein');
    },delaytime);
}

function submit() {
    var name = document.getElementById('name').value;
    var skill = document.getElementById('skill').value;
    var level = document.getElementById('skilloption').value;
    if(name === "" || skill === "" || level ==="") {
        alert("All fields are required!");
    }
    else {
        var tb = document.getElementById('tableinput');
        if(level === "Beginner") {
            tb.innerHTML += ("<tr><td>"+name+"</td><td>"+skill+"</td><td>"+'&#x2714'+"</td><td></td><td></td></tr>");
        } else if(level === "Intermediate") {
            tb.innerHTML += ("<tr><td>"+name+"</td><td>"+skill+"</td><td></td><td>"+'&#x2714'+"</td><td></td></tr>");
        } else {
            tb.innerHTML += ("<tr><td>"+name+"</td><td>"+skill+"</td><td></td><td></td><td>"+'&#x2714'+"</td></tr>");
        }
    }
}

function counter() {
    var countElement = document.getElementById('count');
    var num = countElement.innerHTML;
    num++;
    countElement.innerHTML = num;
}

$(document).ready(function(){
    $(window).scroll(function() {
        $(".anibox").each(function(){
            var pos = $(this).offset().top;
            var winTop = $(window).scrollTop();
            if (pos < winTop + screen.height + 800) {
                $(this).addClass('slideain');
                $(this).removeClass('anibox');
            }
        });
    });
});
