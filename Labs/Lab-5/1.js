function myArray(myarr) {
    var sum=0;
    var product=1;
    this.setSum = function() {
        for(i=0;i<myarr.length;i++) {
            sum+=myarr[i];
        }
    }
    this.getSum = function() {return sum}
    this.setProduct = function() {
        for(i=0;i<myarr.length;i++) {
            product*=myarr[i];
        }
    }
    this.getProduct = function() {return product}
    this.Sort = function() {
        for(i=0;i<myarr.length;i++) {
            for(j=i;j<myarr.length;j++) {
                if(myarr[i]>myarr[j]) {
                    temp = myarr[i];
                    myarr[i] = myarr[j];
                    myarr[j] = temp;
                }
            }
        }
        return myarr;
    }
    this.Modify = function(k, newele) {
        myarr[k] = newele;
        return myarr;
    }
    this.Display = function() {return myarr}
};

var myarr=[9,7,5,3,1];
var arr = new myArray(myarr);
arr.setSum();
arr.setProduct();
console.log(arr.getSum());
console.log(arr.getProduct());
console.log(arr.Sort());
console.log(arr.Display());
console.log(arr.Modify(1,8));