from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    roll = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)

    def __init__(self, roll, name, email):
        self.roll = roll
        self.name = name
        self.email = email

class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(80), unique=True)

    def __init__(self, code, name):
        self.code = code
        self.name = name

@app.route("/students/create", methods=["POST"])
def userAdd():
    roll=request.form['roll']
    name=request.form['name']
    email=request.form['email']
    db.create_all()
    new_person=User(roll, name, email)
    db.session.add(new_person)
    db.session.commit()
    temp ={}
    temp['status']=(type(new_person)==User)
    return jsonify(temp)

@app.route("/courses/create", methods=["POST"])
def courseAdd():
    code=request.form['code']
    name=request.form['name']
    db.create_all()
    new_course=Course(code, name)
    db.session.add(new_course)
    db.session.commit()
    temp ={}
    temp['status']=(type(new_course)==Course)
    return jsonify(temp)

@app.route("/students/", methods=["GET"])
def userFetch():
    db.create_all()
    allUsers=User.query.all()
    strf = '{ Students: ['
    for user in allUsers:
        strf += "{ Rollnumber  " + str(user.roll) + ", Name : " + user.name + ", Email : " + user.email + "}, "
    strf += "]}"
    return jsonify(strf)

@app.route("/courses/", methods=["GET"])
def courseFetch():
    db.create_all()
    allCourses=Course.query.all()
    strf = '{ Courses: ['
    for course in allCourses:
        strf += "{ Code  " + str(course.code) + ", Name : " + course.name + "}, "
    strf += "]}"
    return jsonify(strf)

@app.route("/students/delete", methods=["POST", "GET"])
def deleteUser():
    roll = request.form['roll']
    delete_person=User.query.filter_by(roll=roll).first()
    db.session.delete(delete_person)
    db.session.commit()
    temp ={}
    temp['status']=(type(delete_person)==User)
    return jsonify(temp)

@app.route("/courses/delete", methods=["POST", "GET"])
def deleteCourse():
    code = request.form['code']
    delete_course=Course.query.filter_by(code=code).first()
    db.session.delete(delete_course)
    db.session.commit()
    temp ={}
    temp['status']=(type(delete_course)==Course)
    return jsonify(temp)

@app.route("/students/<rollnumber>", methods=['GET', 'POST'])
def updateUser(rollnumber):
    person = User.query.filter_by(roll=rollnumber).first()
    roll = request.form['roll']
    name=request.form['name']
    email=request.form['email']
    person.roll = roll
    person.name = name
    person.email = email
    db.session.commit()
    strf = '{ Student : { Rollnumber : ' + str(person.roll) + ", Name : " + person.name + ", Email : " + person.email + " } }"
    return jsonify(strf)

@app.route("/courses/<code1>", methods=['GET', 'POST'])
def updateCourse(code1):
    course = Course.query.filter_by(code=code1).first()
    code = request.form['code']
    name = request.form['name']
    course.code = code
    course.name = name
    db.session.commit()
    strf = '{ Course : { Code : ' + str(course.code) + ", name : " + course.name + " } }"
    return jsonify(strf)

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000)
