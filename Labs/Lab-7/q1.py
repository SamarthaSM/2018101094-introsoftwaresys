def isLeapYear(year):
    x = year % 4
    if x == 0:
        return True
    else:
        return False

year = int(input())
print(isLeapYear(year))
