strinp = input()
strout = input()
f = open(strinp, "r")
string = f.read()
string = string.lower()
punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
for x in string.lower():
    if x in punctuations:
        string = string.replace(x, "")
arr = string.split()
w = open(strout, "w")
count = len(arr)
while count != 0:
    temp = arr[0]
    c = 0
    x = 0
    while c < count:
        if arr[c] == temp:
            x+=1
            arr.remove(arr[c])
            count-=1
        else:
            c+=1
    x = str(x)
    w.write(temp+": "+x+"\n")
w.close()
