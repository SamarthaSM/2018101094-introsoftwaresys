WHITE = "white"
BLACK = "black"

class Game:
    def __init__(self):
        self.playersturn = BLACK
        self.message = ""
        self.gameboard = {}
        self.placePieces()
        print("Enter moves in given format : e2(start) e3(end)")
        self.main()


    def placePieces(self):

        for i in range(0,8):
            self.gameboard[(1,i)] = Pawn(WHITE,uniDict[WHITE][Pawn],1)
            self.gameboard[(6,i)] = Pawn(BLACK,uniDict[BLACK][Pawn],-1)

        placers = [Rook,Knight,Bishop,Queen,King,Bishop,Knight,Rook]

        for i in range(0,8):
            self.gameboard[(0,i)] = placers[i](WHITE,uniDict[WHITE][placers[i]])
            self.gameboard[(7,i)] = placers[i](BLACK,uniDict[BLACK][placers[i]])
        placers.reverse()


    def main(self):
        self.message1 = "WHITE's turn!!"
        while True:
            self.printBoard()
            print(self.message)
            print(self.message1)
            startpos,endpos = self.parseInput()
            try:
                target = self.gameboard[startpos]
            except:
                self.message = "Could not find piece!!!"
                target = None

            if target:
                if target.Color != self.playersturn:
                    self.message = "This is not your turn!!!"
                    continue
                if target.isValid(startpos,endpos,target.Color,self.gameboard):
                    self.message = "That is a valid move"
                    self.gameboard[endpos] = self.gameboard[startpos]
                    del self.gameboard[startpos]
                    self.isCheck()
                    if self.playersturn == BLACK:
                        self.playersturn = WHITE
                        self.message1 = "BLACK's turn!!"
                    else :
                        self.playersturn = BLACK
                        self.message1 = "WHITE's turn!!"
                else :
                    self.message = "Invalid move"
            else :
                self.message = "There is no piece in that space"

    def isCheck(self):
        king = King
        kingDict = {}
        pieceDict = {BLACK : [], WHITE : []}
        for position,piece in self.gameboard.items():
            if type(piece) == King:
                kingDict[piece.Color] = position
            pieceDict[piece.Color].append((piece,position))
        if self.canSeeKing(kingDict[WHITE],pieceDict[BLACK]):
            self.message = "White player is in check"
        if self.canSeeKing(kingDict[BLACK],pieceDict[WHITE]):
            self.message = "Black player is in check"


    def canSeeKing(self,kingpos,piecelist):
        for piece,position in piecelist:
            if piece.isValid(position,kingpos,piece.Color,self.gameboard):
                return True

    def parseInput(self):
        try:
            a,b = input().split()
            a = (8-int(a[1]), (ord(a[0])-97))
            b = (8-int(b[1]), (ord(b[0])-97))
            return (a,b)
        except:
            print("error decoding input. please try again")
            return((-1,-1),(-1,-1))

    def printBoard(self):
        for i in range(0,8):
            print(" "+"-"*33)
            print((8-i),end="|")
            for j in range(0,8):
                item = self.gameboard.get((i,j)," ")
                if str(item) == "<0>":
                    print(str(item)+'|', end = "")
                else:
                    print(end = " "+ str(item)+' |')
            print()
        print(" " + "-"*33)
        print("   a   b   c   d   e   f   g   h  ")

class Piece:

    def __init__(self,color,name):
        self.name = name
        self.position = None
        self.Color = color
    def isValid(self,startpos,endpos,Color,gameboard):
        if endpos in self.availableMoves(startpos[0],startpos[1],gameboard, Color = Color):
            return True
        return False
    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def availableMoves(self,x,y,gameboard):
        print("ERROR: no movement for base class")

    def AdNauseum(self,x,y,gameboard, Color, intervals):
        answers = []
        for xint,yint in intervals:
            xtemp,ytemp = x+xint,y+yint
            while self.isInBounds(xtemp,ytemp):
                target = gameboard.get((xtemp,ytemp),None)
                if target is None: answers.append((xtemp,ytemp))
                elif target.Color != Color:
                    answers.append((xtemp,ytemp))
                    break
                else:
                    break

                xtemp,ytemp = xtemp + xint,ytemp + yint
        return answers

    def isInBounds(self,x,y):
        if x >= 0 and x < 8 and y >= 0 and y < 8:
            return True
        return False

    def noConflict(self,gameboard,initialColor,x,y):
        if self.isInBounds(x,y) and (((x,y) not in gameboard) or gameboard[(x,y)].Color != initialColor) : return True
        return False


chessCardinals = [(1,0),(0,1),(-1,0),(0,-1)]
chessDiagonals = [(1,1),(-1,1),(1,-1),(-1,-1)]

def knightList(x,y,int1,int2):
    return [(x+int1,y+int2),(x-int1,y+int2),(x+int1,y-int2),(x-int1,y-int2),(x+int2,y+int1),(x-int2,y+int1),(x+int2,y-int1),(x-int2,y-int1)]
def kingList(x,y):
    return [(x+1,y),(x+1,y+1),(x+1,y-1),(x,y+1),(x,y-1),(x-1,y),(x-1,y+1),(x-1,y-1)]



class Knight(Piece):
    def availableMoves(self,x,y,gameboard, Color = None):
        if Color is None : Color = self.Color
        return [(xx,yy) for xx,yy in knightList(x,y,2,1) if self.noConflict(gameboard, Color, xx, yy)]

class Rook(Piece):
    def availableMoves(self,x,y,gameboard ,Color = None):
        if Color is None : Color = self.Color
        return self.AdNauseum(x, y, gameboard, Color, chessCardinals)

class Bishop(Piece):
    def availableMoves(self,x,y,gameboard, Color = None):
        if Color is None : Color = self.Color
        return self.AdNauseum(x, y, gameboard, Color, chessDiagonals)

class Queen(Piece):
    def availableMoves(self,x,y,gameboard, Color = None):
        if Color is None : Color = self.Color
        return self.AdNauseum(x, y, gameboard, Color, chessCardinals+chessDiagonals)

class King(Piece):
    def availableMoves(self,x,y,gameboard, Color = None):
        if Color is None : Color = self.Color
        return [(xx,yy) for xx,yy in kingList(x,y) if self.noConflict(gameboard, Color, xx, yy)]

class Pawn(Piece):
    def __init__(self,color,name,direction):
        self.name = name
        self.Color = color
        self.direction = direction
    def availableMoves(self,x,y,gameboard, Color = None):
        if Color is None : Color = self.Color
        answers = []
        if (x+self.direction,y+1) in gameboard and self.noConflict(gameboard, Color, x+self.direction, y+1) : answers.append((x+self.direction,y+1))
        if (x+self.direction,y-1) in gameboard and self.noConflict(gameboard, Color, x-self.direction, y-1) : answers.append((x+self.direction,y-1))
        if (x+self.direction,y+2) in gameboard and self.noConflict(gameboard, Color, x+self.direction, y+2) : answers.append((x+self.direction,y+2))
        if (x+self.direction,y-2) in gameboard and self.noConflict(gameboard, Color, x-self.direction, y-2) : answers.append((x+self.direction,y-2))
        if (x+self.direction,y) not in gameboard and Color == self.Color : answers.append((x+self.direction,y))
        return answers

uniDict = {WHITE : {Pawn : "P", Rook : "R", Knight : "K", Bishop : "B", King : "<0>", Queen : "Q" }, BLACK : {Pawn : "P", Rook : "R", Knight : "K", Bishop : "B", King : "<0>", Queen : "Q" }}

Game()
